using System;
using System.Collections.Generic;

namespace Market.Entities
{
    public record Customer
    {
        public Guid Id { get; init; }
        public string Name { get; init; }
        public string Gender { get; init; }
        public double Balance { get; init; }
        public virtual List<Product> Products { get; init; } = new List<Product>();

    }

}
