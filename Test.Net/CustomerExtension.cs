using Market.Entities;
using Market.DTOs;

namespace Market
{
    public static class CustomerExtension
    {
        public static Customer customer;

        public static CustomerDTO AsDto(this Customer customer)
        {
            return new CustomerDTO
            {
                Id = customer.Id,
                Name = customer.Name,
                Gender = customer.Gender,
                Balance = customer.Balance,
                Products = customer.Products
            };
        }
    }
}
