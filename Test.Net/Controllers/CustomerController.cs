using Market.Entities;
using Market.DTOs;
using Market;
using Market.Repositories;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Market.Controllers
{
    // GET /customer
    [ApiController]
    [Route("customer")]
    public class CustomerController : ControllerBase
    {
        private readonly ICustomerRepository repository;


        public CustomerController(ICustomerRepository repository)
        {
            this.repository = repository;
        }

        //GET /customer
        [HttpGet]
        public IEnumerable<CustomerDTO> GetCustomers()
        {
            var customers = repository.GetCustomers().Select(customer => customer.AsDto());
            return customers;
        }

        //GET /customer/{id}
        [HttpGet("{id}")]
        public ActionResult<CustomerDTO> GetCustomer(Guid id)
        {
            var customer = repository.GetCustomer(id);
            if (customer is null)
            {
                return NotFound();
            }
            return customer.AsDto();
        }

        // POST /customer
        [HttpPost]
        public ActionResult<CustomerDTO> CreateCustomer(CreateCustomerDto customerDto)
        {
            Customer customer = new()
            {
                Id = Guid.NewGuid(),
                Name = customerDto.Name,
                Gender = customerDto.Gender,
                Balance = customerDto.Balance,
                Products = customerDto.Products
            };
            repository.CreateCustomer(customer);
            return CreatedAtAction(nameof(GetCustomer), new { id = customer.Id }, customer.AsDto());
        }


        // PUT /customer/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateCustomer(Guid id, UpdateCustomerDTO customerDto)
        {
            var existingCustomer = repository.GetCustomer(id);
            if(existingCustomer is null)
            {
                return NotFound();
            }
            Customer updatedCustomer = existingCustomer with
            {
                Name = customerDto.Name,
                Gender = customerDto.Gender,
                Balance = customerDto.Balance,
                Products = customerDto.Products
            };
            repository.UpdateCustomer(updatedCustomer);
            return NoContent();
        }

        //DELETE /customer/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteCustomer(Guid id)
        {
            var existingCustomer = repository.GetCustomer(id);

            if (existingCustomer is null)
            {
                return NotFound();
            }
            repository.DeleteCustomer(id);
            return NoContent();
        }
    }
}

