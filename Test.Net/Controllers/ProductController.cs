using Market.DTOs;
using Market.Repositories;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using Market;
using Market.Entities;
using System;

namespace Market.Controllers
{
    // GET /product
    [ApiController]
    [Route("product")]
    public class ProductController : ControllerBase
    {
        private readonly IProductRepository repository;
        private readonly ICustomerRepository repositoryCustomer;

        public ProductController(IProductRepository repository, ICustomerRepository repositoryCustomer)
        {
            this.repository = repository;
            this.repositoryCustomer = repositoryCustomer;
        }

        //GET /product
        [HttpGet]
        public IEnumerable<ProductDTO> GetProduct()
        {
            var product = repository.GetProducts().Select(product => product.AsDto());
           
            return product;
        }

        // POST /product
        [HttpPost]
        public ActionResult<ProductDTO> CreateProduct(CreateProductDto productDto)
        {
            Product product = new()
            {
                Id = Guid.NewGuid(),
                IdFK = productDto.IdFK,
                Name = productDto.Name,
                Quantity = productDto.Quantity,
                Price = productDto.Price
            };
            repository.CreateProduct(product);

            //update Customer cu Product
            Guid id = product.IdFK;
            Customer customer = repositoryCustomer.GetCustomer(id);
            customer.Products.Add(product);
            repositoryCustomer.UpdateCustomer(customer);

            return CreatedAtAction(nameof(GetProduct), new { id = product.Id }, product.AsDto());
        }

        //GET /product/{id}
        [HttpGet("{id}")]
        public ActionResult<ProductDTO> GetProduct(Guid id)
        {
            var product = repository.GetProduct(id);
            if (product is null)
            {
                return NotFound();
            }
            return product.AsDto();
        }

        //DELETE /product/{id}
        [HttpDelete("{id}")]
        public ActionResult DeleteProduct(Guid id)
        {
            var existingProduct = repository.GetProduct(id);

            if (existingProduct is null)
            {
                return NotFound();
            }
            repository.DeleteProduct(id);

            // delete pRODUCT from cUSTOMER
            Guid fk = existingProduct.IdFK;
            Customer customer = repositoryCustomer.GetCustomer(fk);
            int index = customer.Products.IndexOf(existingProduct);
            customer.Products.RemoveAt(index);
            repositoryCustomer.UpdateCustomer(customer);

            return NoContent();
        }

        // PUT /product/{id}
        [HttpPut("{id}")]
        public ActionResult UpdateProduct(Guid id, UpdateProductDTO productDto)
        {
            var existingProduct = repository.GetProduct(id);
            if (existingProduct is null)
            {
                return NotFound();
            }
            Product updatedProduct = existingProduct with
            {
                IdFK = productDto.IdFK,
                Name = productDto.Name,
                Quantity = productDto.Quantity,
                Price = productDto.Price
            };
            repository.UpdateProduct(updatedProduct);
            // UPDATE list of Product on Customer
            Guid fkActual = updatedProduct.IdFK;
            Guid fkOld = existingProduct.IdFK;
            Customer customerOld = repositoryCustomer.GetCustomer(fkOld);
            Customer customerActual = repositoryCustomer.GetCustomer(fkActual);
            int indexOld = customerOld.Products.IndexOf(existingProduct);
            customerOld.Products.RemoveAt(indexOld);
            customerActual.Products.Add(updatedProduct);
            repositoryCustomer.UpdateCustomer(customerOld);
            repositoryCustomer.UpdateCustomer(customerActual);
            return NoContent();
        }

    }
}