﻿using Market.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Market.Repositories
{
    public class IInMemProductRepository : IProductRepository
    {
        private readonly List<Product> products = new List<Product>();
        public void CreateProduct(Product product)
        {
            products.Add(product);
        }

        public void DeleteProduct(Guid id)
        {
            var index = products.FindIndex(existingProduct => existingProduct.Id == id);
            products.RemoveAt(index);
        }

        public Product GetProduct(Guid id)
        {
            return products.Where(product => product.Id == id).SingleOrDefault();
        }

        public IEnumerable<Product> GetProducts()
        {
            return products;
        }

        public void UpdateProduct(Product product)
        {
            var index = products.FindIndex(existingProduct => existingProduct.Id == product.Id);
            products[index] = product;
        }
    }
}