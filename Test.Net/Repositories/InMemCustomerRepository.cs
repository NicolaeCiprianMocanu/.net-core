using System;
using System.Collections.Generic;
using System.Linq;
using Market.Entities;

namespace Market.Repositories
{
    public class InMemCustomerRepository : ICustomerRepository
    {
        public static readonly List<Product> list = new List<Product>();
        private readonly List<Customer> customers = new()
        {
            new Customer { Id = Guid.NewGuid(), Name = "Mocanu Ciprian", Gender = "male", Balance = 1200.50 },
            new Customer { Id = Guid.NewGuid(), Name = "Casandrescu Bogdan", Gender = "male", Balance = 700.00 },
            new Customer { Id = Guid.NewGuid(), Name = "Folea Marina", Gender = "female", Balance = 1400.50 }
        };

        public IEnumerable<Customer> GetCustomers()
        {
            return customers;
        }

        public Customer GetCustomer(Guid id)
        {

            return customers.Where(customer => customer.Id == id).SingleOrDefault();
        }

        public void CreateCustomer(Customer customer)
        {
            customers.Add(customer);
        }

        public void UpdateCustomer(Customer customer)
        {
            var index = customers.FindIndex(existingCustomer => existingCustomer.Id == customer.Id);
            customers[index] = customer;
        }

        public void DeleteCustomer(Guid id)
        {
            var index = customers.FindIndex(existingCustomer => existingCustomer.Id == id);
            customers.RemoveAt(index);
        }
    }
}