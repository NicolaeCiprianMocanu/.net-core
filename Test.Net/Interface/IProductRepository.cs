using System;
using System.Collections.Generic;
using System.Linq;
using Market.Entities;

namespace Market.Repositories
{
    public interface IProductRepository
    {

        Product GetProduct(Guid id);
        IEnumerable<Product> GetProducts();
        void CreateProduct(Product product);
        void UpdateProduct(Product product);
        void DeleteProduct(Guid id);
    }
}