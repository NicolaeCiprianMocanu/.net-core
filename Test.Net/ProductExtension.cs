using Market.Entities;
using Market.DTOs;

namespace Market
{
    public static class ProductExtension
    {
        public static Product product;

        public static ProductDTO AsDto(this Product product)
        {
            return new ProductDTO
            {
                Id = product.Id,
                IdFK = product.IdFK,
                Name = product.Name,
                Quantity = product.Quantity,
                Price = product.Price
            };
        }
    }
}

