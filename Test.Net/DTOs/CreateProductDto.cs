using System;
using System.ComponentModel.DataAnnotations;

namespace Market.DTOs
{
    public class CreateProductDto {
        [Required]
        public Guid IdFK { get; init; }
        [Required]
        public string Name { get; init; }
        [Required]
        public decimal Quantity { get; init; }
        [Required]
        public decimal Price { get; init; }
    }

}
