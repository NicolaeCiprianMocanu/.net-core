using System;

namespace Market.DTOs
{
    public class ProductDTO
    {
        public Guid Id { get; init; }
        public Guid IdFK { get; init; }
        public string Name { get; init; }
        public decimal Quantity { get; init; }
        public decimal Price { get; init; }
    }
}