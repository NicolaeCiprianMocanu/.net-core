using Market.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Market.DTOs
{
    public record UpdateProductDTO
    {
        [Required]
        public Guid IdFK { get; init; }
        [Required]
        public string Name { get; init; }
        [Required]
        public decimal Quantity { get; init; }
        [Required]
        public decimal Price { get; init; }
    }
}
