using Market.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Market.DTOs
{
    public record UpdateCustomerDTO
    {
        [Required]
        public string Name { get; init; }
        [Required]
        public string Gender { get; init; }
        [Required]
        public double Balance { get; init; }
        public virtual List<Product> Products { get; init; } = new List<Product>();
    }
}
